#import "ExampleImagePickerPlugin.h"
#if __has_include(<example_image_picker/example_image_picker-Swift.h>)
#import <example_image_picker/example_image_picker-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "example_image_picker-Swift.h"
#endif

@implementation ExampleImagePickerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftExampleImagePickerPlugin registerWithRegistrar:registrar];
}
@end
