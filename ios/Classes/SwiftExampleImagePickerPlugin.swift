import Flutter
import UIKit

public class SwiftExampleImagePickerPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "pickImage", binaryMessenger: registrar.messenger(),codec: FlutterStandardMethodCodec.sharedInstance())
        let instance = SwiftExampleImagePickerPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
}


