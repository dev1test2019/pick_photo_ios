import 'package:flutter_test/flutter_test.dart';
import 'package:example_image_picker/example_image_picker.dart';
import 'package:example_image_picker/example_image_picker_platform_interface.dart';
import 'package:example_image_picker/example_image_picker_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockExampleImagePickerPlatform 
    with MockPlatformInterfaceMixin
    implements ExampleImagePickerPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final ExampleImagePickerPlatform initialPlatform = ExampleImagePickerPlatform.instance;

  test('$MethodChannelExampleImagePicker is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelExampleImagePicker>());
  });

  test('getPlatformVersion', () async {
    ExampleImagePicker exampleImagePickerPlugin = ExampleImagePicker();
    MockExampleImagePickerPlatform fakePlatform = MockExampleImagePickerPlatform();
    ExampleImagePickerPlatform.instance = fakePlatform;
  
    expect(await exampleImagePickerPlugin.getPlatformVersion(), '42');
  });
}
