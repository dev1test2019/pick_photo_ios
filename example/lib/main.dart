import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class CardView extends StatelessWidget {
  final Widget child;
  final Color borderColor;
  final double borderWidth;
  final Color backgroundColor;
  final Color highlightColor;
  final double radius;
  final List<BoxShadow>? boxShadow;
  final EdgeInsetsGeometry? padding;
  final void Function()? onTap;

  const CardView({
    Key? key,
    this.child = const SizedBox(),
    this.borderColor = Colors.transparent,
    this.borderWidth = 0,
    this.backgroundColor = Colors.white,
    this.highlightColor = Colors.white,
    this.radius = 10,
    this.boxShadow,
    this.padding,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.all(Radius.circular(radius)),
      highlightColor: highlightColor,
      child: Container(
        padding: padding ?? EdgeInsets.zero,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(radius)),
          color: backgroundColor,
          border: Border.all(
            color: borderColor,
            width: borderWidth,
          ),
          boxShadow: boxShadow,
        ),
        child: child,
      ),
    );
  }
}
