//
//  ImagePickerController.swift
//  Runner
//
//  Created by MACBOOK on 9/14/22.
//

import Foundation

class ImagePickerController: UIImagePickerController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var handler: ((_ image: UIImage?) -> Void)?
    
    convenience init(sourceType: UIImagePickerController.SourceType, handler: @escaping (_ image: UIImage?) -> Void) {
        self.init()
        self.sourceType = sourceType
        self.delegate = self
        self.handler = handler
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        handler?(info[UIImagePickerController.InfoKey.originalImage] as? UIImage)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        handler?(nil)
    }
}
